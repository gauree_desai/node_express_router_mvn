//import model
const restaurants=require('../models/restaurant.json')

//controller will have mapping functions 
exports.getAllRestaurants=(req,res)=>{
  res.status(200).json({ 
            message:"restaurants fetched successfully" , 
            restaurantList:restaurants
          })
}

exports.getRestaurantByName=(req,res)=>{
    const filteredRestaurants=restaurants.filter((item)=> item.name==req.params.name);
    filteredRestaurants.length ?
    res.status(200).json({
        message:"restaurants by name fetched successfully" , 
        restaurantList: filteredRestaurants
    }):
    res.status(200).json({
        message:"restaurants by name fetched successfully 0 records fetched" , 
    })
}

exports.addRestaurant=(req,res)=>{
  restaurants.push(req.body)
  //FileSystem.writefile() as a homework
  res.status(200).json({
    message:"restaurants added successfully" , 
    restaurantList: restaurants
})
}

exports.updateRestaurant=(req,res)=>{
    const index=restaurants.findIndex((item)=>item.name==req.body.name)
    restaurants[index].city=req.body.city
    res.status(200).json({
        message:"restaurants updated successfully" , 
        restaurantList: restaurants
    })
    
 }


 exports.deleteRestaurant=(req,res)=>{
    const index=restaurants.findIndex((item)=>item.id==req.params.id)
    restaurants.splice(index,1);
    res.status(200).json({
        message:"restaurant deleted successfully" , 
        restaurantList: restaurants
    })

 }
