const express=require('express')
const restaurantController=require('../controllers/restaurant')



const router=express.Router();


//configure route
router.get('/:name',restaurantController.getRestaurantByName)
router.get('',restaurantController.getAllRestaurants)
router.post('',restaurantController.addRestaurant)
router.put('',restaurantController.updateRestaurant)
router.delete('/:id',restaurantController.deleteRestaurant)











module.exports=router;