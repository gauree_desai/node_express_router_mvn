//import
const express=require('express')
const bodyParser=require('body-parser')
const restaurantRoutes=require('./routes/restaurants')

//create server
var app=express()

//constants
const PORT= 6767
const log=console.log


//middleware
app.use(bodyParser.json())
app.use('/restaurant',restaurantRoutes)







//last stmt listen
app.listen(PORT,()=>{
  log(`app is running on ${PORT}`)
})